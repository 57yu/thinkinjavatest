package ch21_Concurrency.ThreadLocal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConcurrencyException {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		Iterator<String> iter = list.iterator();
		while (iter.hasNext()) {
			String s = iter.next();
			System.out.println(s);
			if (s.equals("c")) {
				list.add("e");
			}
		}
		/*for (String s : list) {
			System.out.println(s);
			if (s.equals("c")) {
				list.add("e");
			}
		}*/
	}
}
