package InvocationHandler.practice;

/**
 * 为毛答案是方法运行时间，书上翻译却是统计调用次数
 */
import static net.mindview.util.Print.print;

interface Interface {
	void doSomething();

	void somethingElse(String arg);
}

class RealObject implements Interface {
	public void doSomething() {
		print("doSomething");
	}

	public void somethingElse(String arg) {
		print("somethingElse" + arg);
	}
}

class SimpleProxy implements Interface {

	public static int something_count = 0;
	public static int somethingElse_count = 0;

	private Interface proxied;

	public SimpleProxy(Interface proxied) {
		this.proxied = proxied;
	}

	public void doSomething() {
		print("SimpleProxy doSomething");
		proxied.doSomething();
		something_count++;
	}

	public void somethingElse(String arg) {
		print("SimpleProxy somethingElse");
		proxied.somethingElse(arg);
		somethingElse_count++;
	}
}

/**
 * @author yuchao
 *
 */
public class SimpleProxyDemo {
	public static void consumer(Interface iface) {
		iface.doSomething();
		iface.somethingElse("57yu");
	}

	public static void main(String[] args) {
		consumer(new RealObject());
		SimpleProxy sp = new SimpleProxy(new RealObject());
		consumer(sp);
		print("Something count " + SimpleProxy.something_count);
		print("SomethingElse count " + SimpleProxy.somethingElse_count);
	}
}
