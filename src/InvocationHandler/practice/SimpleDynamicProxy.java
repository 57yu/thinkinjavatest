package InvocationHandler.practice;

import static net.mindview.util.Print.print;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

class DynamicProxyHandler implements InvocationHandler {
	private Object proxied;

	public DynamicProxyHandler(Object proxied) {
		this.proxied = proxied;
	}

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		print("*** proxy: " + proxy.getClass() + ", method: " + method + ", args: " + args);
		print("s--------------------");
		print("proxy " + proxy);
		print("e--------------------");

		if (args != null) {
			for (Object arg : args) {
				print(" " + arg);
			}
		}
		long start = System.nanoTime();
		Object ret = method.invoke(proxied, args);
		print("Method takes: " + (System.nanoTime() - start));
		return ret;
	}
}

public class SimpleDynamicProxy {
	public static void consumer(Interface iface) {
		iface.doSomething();
		iface.somethingElse("57yu");
	}

	public static void main(String[] args) {
		RealObject real = new RealObject();
		consumer(real);
		Interface proxy = (Interface) Proxy.newProxyInstance(Interface.class.getClassLoader(),
				new Class[] { Interface.class }, new DynamicProxyHandler(real));
		consumer(proxy);
	}
}
