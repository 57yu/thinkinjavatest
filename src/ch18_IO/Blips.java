package ch18_IO;

import java.io.Externalizable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import static net.mindview.util.Print.*;

class Blip1 implements Externalizable {

	public Blip1() {
		print("Blip1 Contstructor");
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		print("Blip1.writeExternal");
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		print("Blip2.readExternal");
	}
	
}

class Blip2 implements Externalizable {

	Blip2() {
		print("Blip2 Constructor");
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		print("Blip2.writeExternal");
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		print("Blip2.readExternal");
	}
	
}

public class Blips {
	public static void main(String[] args) throws  IOException, ClassNotFoundException {
		print("Constructing objects:");
		Blip1 b1 = new Blip1();
		Blip2 b2 = new Blip2();
		ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("D:/worm.out"));
		print("Saving objects");
		o.writeObject(b1);
		o.writeObject(b2);
		o.close();
		// now get them back
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("D:/worm.out"));
		print("Recovering b1:");
		b1 = (Blip1) in.readObject();
		//OOPS throws an exception
		b2 = (Blip2) in.readObject();
		
	}
}
