package ch18_IO.practice;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author YUCHAO
 * @date 2013-6-12 23:54
 */
public class ch18_15 {
	public static void main(String[] args) throws IOException {
		DataOutputStream out = new DataOutputStream(new FileOutputStream("D:/text.txt"));
		out.writeDouble(3.14159);
		out.writeUTF("That was pi");
		out.writeDouble(1.41413);
		out.writeUTF("Square root of 2");
		out.writeBoolean(true);
		out.writeInt(11);
		out.close();
		DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream("D:/text.txt")));
		System.out.println(in.readDouble());
		System.out.println(in.readUTF());
		System.out.println(in.readDouble());
		System.out.println(in.readUTF());
		System.out.println(in.readBoolean());
		System.out.println(in.readInt());
	}
}
