package ch18_IO.practice;

import static net.mindview.util.Print.print;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.regex.Pattern;

public class ch18_03 {
	public static void main(String[] args) {
		File path = new File("E:/workSpace/HDEAP2.0/src/main/java/com/goodwill/hdeap/dao");
		String[] list;
		if (args.length == 0)
			list = path.list();
		else
			list = path.list(new DirFilter03(args[0]));
		Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
		long total = 0;
		long fs = 0;
		for (String dirItem : list) {
			fs = new File(path, dirItem).length();
			System.out.println(dirItem + ", " + fs + " byte(s)");
			total += fs;
		}
		print("==================");
		print("Total:" + total);
	}
}

class DirFilter03 implements FilenameFilter {
	private Pattern pattern;

	public DirFilter03(String regex) {
		pattern = Pattern.compile(regex);
	}

	@Override
	public boolean accept(File dir, String name) {
		return pattern.matcher(name).matches();
	}

}
