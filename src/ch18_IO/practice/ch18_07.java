package ch18_IO.practice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @description 读取文件存入list中
 * @author YUCHAO
 * @date 2013-6-11 22:54
 */
public class ch18_07 {
	
	public static List<String> read(String filename) throws IOException{
		List<String> list = new LinkedList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String s;
		while ((s = reader.readLine()) != null)
			list.add(s);
		reader.close();
		return list;
	}
	
	public static void main(String[] args) throws IOException {
		List<String> list = read("C:/Users/Administrator/git/thinkinjavatest/ThinkInJavaTest/src/ch18_IO/BufferedInputFile.java");
		for (ListIterator<String> it = list.listIterator(list.size()); it.hasPrevious(); )
			System.out.println(it.previous());
	}
}
