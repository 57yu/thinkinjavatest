package ch18_IO.practice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.List;

import ch18_IO.BufferedInputFile;

/**
 * @description 比较有缓冲和无缓冲的I/O方式在向文件写入时的性能差别
 * @author YUCHAO
 * @date 2013-6-11 22:43
 */
public class ch18_14 {
	static String file = "ch18_14.java";
	public static void main(String[] args) throws IOException {
		
		List<String> list = ch18_07.read("C:/Users/Administrator/git/thinkinjavatest/ThinkInJavaTest/src/ch18_IO/BufferedInputFile.java");
		
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		int lineCount = 1;
		long t1 = System.currentTimeMillis();
		for (int i = 0 ; i < 1000; i++) 
			for(String s : list){
				out.println(lineCount++ + ": " + s);
			}
		long t2 = System.currentTimeMillis();
		System.out.println("buffered " + (t2 - t1));
		out.close();
		out = new PrintWriter(new FileWriter(file));
		lineCount = 1;
		t1 = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++)
			for(String s : list){
				out.println(lineCount++ + ": " + s);
			}
		t2 = System.currentTimeMillis();
		System.out.println("unbuffered " + (t2 - t1));
	}
}
