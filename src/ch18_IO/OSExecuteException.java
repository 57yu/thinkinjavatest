package ch18_IO;

public class OSExecuteException extends RuntimeException {
	public OSExecuteException(String why) {
		super(why);
	}
}
