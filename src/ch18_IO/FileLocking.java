package ch18_IO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;
import java.util.concurrent.TimeUnit;

public class FileLocking {
	public static void main(String[] args) throws Exception{
		FileOutputStream fos = new FileOutputStream("D:/text.txt");
		FileLock fl = fos.getChannel().tryLock();
		if (fl != null) {
			System.out.println("Locked File");
			TimeUnit.MILLISECONDS.sleep(100);
			System.out.println("lock's type " + fl.isShared());
			fl.release();
			System.out.println("Released Lock");
		}
		fos.close();
	}
}
