package ch18_IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * @author yuchao
 * Using transferTo() between channels
 */
public class TransferTo {
	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
			System.out.println("aregument : soucrefile destfile");
			System.exit(1);
		}
		FileChannel
			in = new FileInputStream(args[0]).getChannel(),
			out = new FileOutputStream(args[1]).getChannel();
		in.transferTo(0, in.size(), out);
		// Or:
		//out.transferFrom(in, 0, in.size());
	}
}
