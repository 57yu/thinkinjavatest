package ch18_IO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @description �����������ļ�
 * @author yuhao
 * @date 2013-6-10 20:14
 */
public class BufferedInputFile {
	public static String read(String filename) throws IOException {
		//Reading input by lines
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String s;
		StringBuilder sb = new StringBuilder();
		while ((s=in.readLine()) !=null) {
			sb.append(s + "\n");
		}
		in.close();
		return sb.toString();
	}
	
	public static void main(String[] args) throws IOException{
		System.out.println(read("C:/Users/Administrator/git/thinkinjavatest/ThinkInJavaTest/src/ch18_IO/BufferedInputFile.java"));
	}
}
