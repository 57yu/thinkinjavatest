package ch18_IO;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @description 一次一个字节符地读取字符  使用available方法查看还有多少可供存取的字符
 * @author yuchao
 * @date 2013-6-10 23:52
 */
public class TestEOF {
	public static void main(String[] args) throws IOException {
		DataInputStream in = new DataInputStream(new FileInputStream("C:/Users/Administrator/git/thinkinjavatest/ThinkInJavaTest/src/ch18_IO/BufferedInputFile.java"));
		while (in.available() != 0)
			System.out.print((char)in.readByte());
		
	}
}
