package ch18_IO;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 * @description 格式化的内存输入
 * @author yuchao
 * @date 2013-6-10 22:55
 *
 */
public class FormattedMemoryInput {
	public static void main(String[] args) throws IOException {
		try {
			DataInputStream in = new DataInputStream(new ByteArrayInputStream(BufferedInputFile.read("C:/Users/Administrator/git/thinkinjavatest/ThinkInJavaTest/src/ch18_IO/BufferedInputFile.java").getBytes()));
			while (true) {
				System.out.print((char)in.readByte());
			}
		} catch (EOFException e) {
			System.out.println("End of stream");
		}
	}
}
