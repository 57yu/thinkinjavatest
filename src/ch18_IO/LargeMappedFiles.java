package ch18_IO;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import static net.mindview.util.Print.*;

/**
 * @author yuchao
 * Creating a very large file using mapping
 *  the length is up to ur
 */
public class LargeMappedFiles {
	static int length = 0x8FFFFFF; // 128M
	public static void main(String[] args) throws IOException {
		MappedByteBuffer out = new RandomAccessFile("D:/test.txt", "rw").getChannel()
				.map(FileChannel.MapMode.READ_WRITE, 0, length);
		for (int i = 0; i < length; i++)
			out.put((byte)'x');
		print("Finished writing");
		for (int i = length /2; i < length / 2 + 1000; i++)
			printnb((char)out.get(i));
	}
}
