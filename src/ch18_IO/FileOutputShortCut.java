package ch18_IO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;

/**
 * @description 文本文件输出的快捷方式
 * @author yuchao
 * @date 2013-6-11 22:23
 */
public class FileOutputShortCut {
	static String file = "FileOutputShortCut.java";
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new StringReader(BufferedInputFile.read("FileOutputShortCut.java")));
		PrintWriter out = new PrintWriter(file);
		int lineCount = 1;
		String s;
		while ((s = in.readLine()) != null) {
			out.println(lineCount++ + ":"  + s);
		}
		out.close();
		System.out.println(BufferedInputFile.read(file));
	}
}
