package ch5_Init_cleanup.practice;

class StringR {
	String s = "Hello";

	public StringR() {

	}
}

class StringRR {
	String s;

	public StringRR(String s) {
		this.s = s;
	}
}

public class ch05_01 {
	public static void main(String[] args) {
		System.out.println(new StringR().s);
		System.out.println(new StringRR("World").s);
	}
}
