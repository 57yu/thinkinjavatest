package ch11.practice;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

class Command {
	String s;

	public Command(String s) {
		this.s = s;
	}

	public void operation() {
		System.out.println("String: " + s);
	}
}

class Producer {
	public static void produce(Queue<Command> queue) {
		Random rand = new Random(47);
		for (int i = 0; i < 10; i++) {
			queue.offer(new Command(rand.nextInt(10) + ""));
		}
	}
}

class Consumer {
	public static void consumer(Queue<Command> queue) {
		while (queue.peek() != null)
			queue.remove().operation();
	}
}

public class ch_27 {
	public static void main(String[] args) {
		Queue<Command> cmds = new LinkedList<Command>();
		Producer.produce(cmds);
		Consumer.consumer(cmds);
	}
}
