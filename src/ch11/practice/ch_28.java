package ch11.practice;

import java.util.PriorityQueue;
import java.util.Random;

public class ch_28 {
	public static void main(String[] args) {
		PriorityQueue<Double> priorityQueue = new PriorityQueue<Double>();
		Random rand = new Random(47);
		for (int i = 0; i < 10; i++)
			priorityQueue.offer(rand.nextDouble());
		while (priorityQueue.peek() != null)
			System.out.println(priorityQueue.poll());
	}
}
