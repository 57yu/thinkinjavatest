package ch17_Container_in_depth;

public class Pair<K, V> {
	public final K key;
	public final V value;

	public Pair(K k, V v) {
		key = k;
		value = v;
	}
}
