package ch17_Container_in_depth;

import static net.mindview.util.Print.print;

import java.util.Iterator;

import net.mindview.util.Generator;

class Letters implements Generator<Pair<Integer, String>>, Iterable<Integer> {
	private int size = 9;
	private int number = 1;
	private char letter = 'A';

	@Override
	public Pair<Integer, String> next() {
		return new Pair<Integer, String>(number++, "" + letter++);
	}

	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			public Integer next() {
				return number++;
			}

			public boolean hasNext() {
				return number < size;
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}

		};
	}

}

public class MapDataTest {
	public static void main(String[] args) {
		print(MapData.map(new Letters(), 11));

		print(MapData.map(new Letters(), "pop"));
	}
}
