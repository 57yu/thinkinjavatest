package ch17_Container_in_depth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class StringAddress {
	private String s;

	public StringAddress(String s) {
		this.s = s;
	}

	@Override
	public String toString() {
		return super.toString() + " " + s;
	}
}

/**
 * @description fill the container 填充实体
 * @author yuchao
 * @date 2013-7-29
 */
public class FillingLists {
	public static void main(String[] args) {
		List<StringAddress> list = new ArrayList<StringAddress>(Collections.nCopies(1, new StringAddress("Hello")));
		System.out.println(list);
		//fill方法复制同一个对象引用来填充整个容器（只替换）
		Collections.fill(list, new StringAddress("Test"));
		System.out.println(list);
	}
}
