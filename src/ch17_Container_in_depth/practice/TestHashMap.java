package ch17_Container_in_depth.practice;

import java.util.HashMap;
import java.util.Map;

public abstract class TestHashMap {
	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<Integer, String>(1);
		map.put(1, "a");
		//map.put(1, "b");
		map.put(null, "b");
		map.put(null, "c");
		System.out.println(map.get(null));
		System.out.println(map.get(1));
	}
}
