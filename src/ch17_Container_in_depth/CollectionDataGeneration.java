package ch17_Container_in_depth;

import java.util.ArrayList;
import java.util.HashSet;

import net.mindview.util.RandomGenerator;

/**
 * @author yuchao
 * @date 2013-7-29
 */
public class CollectionDataGeneration {
	public static void main(String[] args) {
		System.out.println(new ArrayList<String>(CollectionData.list(new RandomGenerator.String(9), 10)));
		System.out.println(new HashSet<Integer>(new CollectionData<Integer>(new RandomGenerator.Integer(), 10)));
	}
}
