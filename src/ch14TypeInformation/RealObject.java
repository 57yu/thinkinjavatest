package ch14TypeInformation;

public class RealObject implements Interface {
	public void doSomething() {
		System.out.print("doSomething");
	}

	public void somethingElse(String arg) {
		System.out.println("somethingElse " + arg);
	}

}
