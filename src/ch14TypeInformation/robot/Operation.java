package ch14TypeInformation.robot;

public interface Operation {
	String description();

	void command();
}
