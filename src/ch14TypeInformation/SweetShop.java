package ch14TypeInformation;

import static net.mindview.util.Print.print;

class Candy {
	static {
		print("Loading Candy");
	}
}

class Gum {
	static {
		print("Loading Gum");
	}
}

class Cookie {
	static {
		print("Loading cookie");
	}
}

/**
 * Examination of the way the class loader works.
 * @author yuchao
 *
 */
public class SweetShop {
	public static void main(String[] args) {
		print("Inside main");
		new Candy();
		print("Affter create Candy");
		try {
			Class.forName("ch14TypeInformation.Gum");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			print("Could not find Gum");
		}
		print("After class.forName(\"Gum\")");
		new Cookie();
		print("After create cookie");
	}
}
