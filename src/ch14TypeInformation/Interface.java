package ch14TypeInformation;

public interface Interface {
	void doSomething();

	void somethingElse(String args);
}
